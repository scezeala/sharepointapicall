﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharepointAPICall.Model
{
    public class Data
    {
        public Result d { get; set; }
    }

    public class Result
    {
        public Rates[] results { get; set; }
    }

    public class Rates
    {
        public int Id { get; set; }

        public string yopa { get; set; }

        public string pdik { get; set; }

        public string PD_x0020_Closing_x0020_Rate { get; set; }

        public string Rate { get; set; }
    }
}
