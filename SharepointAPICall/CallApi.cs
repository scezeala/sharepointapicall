﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using SharepointAPICall.Model;
using Newtonsoft.Json;
using System.Net.Security;

namespace SharepointAPICall
{
    public class CallApi
    {
        private static AppSettingsReader appSettingsReader;

        static CallApi()
        {
            appSettingsReader = new AppSettingsReader();
        }
        public static async Task<Data> GetToken(
                      string userName,
                      string password,
                      string endpoint)
        {
            Data _data = new Data();
            try
            {
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (RemoteCertificateValidationCallback)((se, cert, chain, sslerror) => true);
                string endpointUrl2 = endpoint;
                NetworkCredential credentials = new NetworkCredential(userName, password);
                using (HttpClient httpClient = new HttpClient((HttpMessageHandler)new HttpClientHandler()
                {
                    Credentials = (ICredentials)credentials
                }))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json;odata=verbose"));
                    HttpResponseMessage response = await httpClient.GetAsync(endpointUrl2);
                    if (response.IsSuccessStatusCode)
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        _data = JsonConvert.DeserializeObject<Data>(apiResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _data;
        }
    }
}
