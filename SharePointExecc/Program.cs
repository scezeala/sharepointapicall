﻿using SharepointAPICall;using SharepointAPICall.Model;using System;using System.Collections.Generic;using System.Configuration;using System.Data;using System.Data.SqlClient;using System.Linq;using System.Reflection;

namespace SharePointExecc
{
    class Program
    {
        private static AppSettingsReader appSettingsReader;

        private static void Main(string[] args)
        {
            Console.WriteLine("Application Started");
            Program.appSettingsReader = new AppSettingsReader();
            string str1 = Program.appSettingsReader.GetValue("connectionString", typeof(string)) as string;
            string str2 = Program.appSettingsReader.GetValue("tableName", typeof(string)) as string;
            string userName = Program.appSettingsReader.GetValue("_username", typeof(string)) as string;
            string encryptedText = Program.appSettingsReader.GetValue("_password", typeof(string)) as string;
            string endpoint = Program.appSettingsReader.GetValue("endpointUrl2", typeof(string)) as string;
            string key = Program.appSettingsReader.GetValue("Key", typeof(string)) as string;
            string password = Encrypter.doEncryptAES(encryptedText, key);
            try
            {
                SharepointAPICall.Model.Data result1 = CallApi.GetToken(userName, password, endpoint).Result;
                ((IEnumerable<Rates>)result1.d.results).Count<Rates>();
                List<Rate> rateList = new List<Rate>();
                foreach (Rates result2 in result1.d.results)
                    rateList.Add(new Rate()
                    {
                        currency = result2.Rate,
                        buyRate = result2.yopa,
                        sellRate = result2.pdik,
                        midRate = result2.PD_x0020_Closing_x0020_Rate
                    });
                if (rateList.Any<Rate>())
                {
                    int index = 0;
                    DataTable table = Program.ReadT464ForBP(rateList);
                    if (!Program.IsTableExist(str1, str2))
                    {
                        string str3 = " CREATE TABLE [dbo].[" + str2 + "]( ";
                        foreach (DataColumn column in (InternalDataCollectionBase)table.Columns)
                            str3 = str3 + "[" + column.ToString() + "] [varchar](max) NULL,";
                        string query = str3 + ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
                        Program.CreateDatabaseTable(str1, query);
                    }
                    Logger.Log((Exception)null, "checking if columns exist. ");
                    string[] columnsName = Program.getColumnsName(str1, str2);
                    bool flag = false;
                    if ((uint)columnsName.Length > 0U)
                    {
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(str1))
                        {
                            foreach (DataColumn column in (InternalDataCollectionBase)table.Columns)
                            {
                                if (columnsName[index].ToString() == column.ColumnName.ToString())
                                {
                                    sqlBulkCopy.ColumnMappings.Add(column.ColumnName.ToString(), column.ColumnName.ToString());
                                }
                                else
                                {
                                    Logger.Log((Exception)null, "column mismatch");
                                    flag = true;
                                }
                                ++index;
                            }
                            if (flag)
                                return;
                            Logger.Log((Exception)null, "Writing to Table");
                            sqlBulkCopy.BulkCopyTimeout = 700;
                            sqlBulkCopy.DestinationTableName = str2;
                            sqlBulkCopy.WriteToServer(table);
                            Logger.Log((Exception)null, "Completed");
                        }
                    }
                    else
                        Logger.Log((Exception)null, "This table has not been created in the DB. Create a table for records.");
                }
                else
                    Logger.Log((Exception)null, "No rates Returned.");
            }
            catch (Exception ex)
            {
                Logger.Log((Exception)null, "An Error Occurred");
                Logger.Log(ex, (string)null);
            }
        }

        private static DataTable ReadT464ForBP(List<Rate> rate)
        {
            int num = 0;
            DataTable dataTable = new DataTable();
            foreach (Rate rate1 in rate)
            {
                foreach (PropertyInfo property in rate1.GetType().GetProperties())
                {
                    if (num == 0)
                        dataTable.Columns.Add(new DataColumn()
                        {
                            DataType = typeof(string),
                            ColumnName = property.Name,
                            AllowDBNull = true
                        });
                    else
                        break;
                }
                dataTable.Rows.Add((object)rate1.currency, (object)rate1.buyRate, (object)rate1.sellRate, (object)rate1.midRate);
                ++num;
            }
            return dataTable;
        }

        private static bool IsTableExist(string connString, string table)
        {
            bool flag = false;
            string cmdText = "SELECT COUNT(*) as 'TableCount' FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA = 'dbo'  AND TABLE_NAME = '" + table + "' ";
            using (SqlConnection connection = new SqlConnection(connString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(cmdText, connection))
                {
                    connection.Open();
                    sqlCommand.ExecuteNonQuery();
                    flag = Convert.ToInt32(sqlCommand.ExecuteScalar()) > 0;
                    connection.Close();
                }
            }
            return flag;
        }

        private static void CreateDatabaseTable(string connString, string query)
        {
            string cmdText = query;
            using (SqlConnection connection = new SqlConnection(connString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(cmdText, connection))
                {
                    connection.Open();
                    sqlCommand.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        private static string[] getColumnsName(string Connection, string tableName)
        {
            List<string> stringList = new List<string>();
            using (SqlConnection sqlConnection = new SqlConnection(Connection))
            {
                using (SqlCommand command = sqlConnection.CreateCommand())
                {
                    command.CommandText = "select c.name from sys.columns c inner join sys.tables t on t.object_id = c.object_id and t.name = '" + tableName + "'";
                    sqlConnection.Open();
                    using (SqlDataReader sqlDataReader = command.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                            stringList.Add(sqlDataReader.GetString(0));
                    }
                }
            }
            return stringList.ToArray();
        }
    }
}
