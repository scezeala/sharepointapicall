﻿using System;
using System.IO;
using System.Reflection;

namespace SharePointExecc
{
    public class Logger
    {
        public Logger()
        {
        }

       private static int LogFileSize = int.Parse("LOG_FILE_SIZE".GetKeyValue());

        public static void Log(Exception ex = null, string info = null)
        {
            string empty = string.Empty;
            string str1 = info == null ? "error" : nameof(info);
            string path = Path.Combine(new FileInfo(Assembly.GetExecutingAssembly().Location).DirectoryName, str1 + "_log.txt");
            if (File.Exists(path))
            {
                FileInfo fileInfo = new FileInfo("LOG_PATH".GetKeyValue() + str1 + "_log.txt");
                if (fileInfo.Length > (long)(Logger.LogFileSize * 1024 * 1024))
                    fileInfo.MoveTo("LOG_PATH".GetKeyValue() + str1 + "_log_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
            }
            string str2 = ex == null ? info : string.Format("An error occurred Exception Message : {0} with stack trace : {1} and Inner Message : {2}", (object)ex.Message, (object)ex.StackTrace, (object)ex.InnerException);
            File.AppendAllText(path, DateTime.Now.ToString() + " " + str2 + Environment.NewLine);
            if (info == null)
                return;
            Console.WriteLine(info);
        }
    }
}
