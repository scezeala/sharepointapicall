﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SharePointExecc
{
    public class Encrypter
    {
        public Encrypter()
        {
        }

        public static string doEncryptAES(string plainText, string key)
        {
            return Convert.ToBase64String(Encrypter.Encrypt(Encoding.UTF8.GetBytes(plainText), Encrypter.getRijndaelManaged(key)));
        }

        public static string doDecryptAES(string encryptedText, string key)
        {
            return Encoding.UTF8.GetString(Encrypter.Decrypt(Convert.FromBase64String(encryptedText), Encrypter.getRijndaelManaged(key)));
        }

        private static RijndaelManaged getRijndaelManaged(string secretKey)
        {
            byte[] numArray = new byte[16];
            byte[] bytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy((Array)bytes, (Array)numArray, Math.Min(numArray.Length, bytes.Length));
            RijndaelManaged rijndaelManaged = new RijndaelManaged();
            rijndaelManaged.Mode = CipherMode.CBC;
            rijndaelManaged.Padding = PaddingMode.PKCS7;
            rijndaelManaged.KeySize = 128;
            rijndaelManaged.BlockSize = 128;
            rijndaelManaged.Key = numArray;
            rijndaelManaged.IV = numArray;
            return rijndaelManaged;
        }

        private static byte[] Encrypt(byte[] plainBytes, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateEncryptor().TransformFinalBlock(plainBytes, 0, plainBytes.Length);
        }

        private static byte[] Decrypt(byte[] encryptedData, RijndaelManaged rijndaelManaged)
        {
            return rijndaelManaged.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        }
    }
}
