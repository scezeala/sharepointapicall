﻿using System;
namespace SharePointExecc
{
    public class Rate
    {
        public Rate()
        {
        }

        public string currency { get; set; }

        public string buyRate { get; set; }

        public string sellRate { get; set; }

        public string midRate { get; set; }
    }
}
