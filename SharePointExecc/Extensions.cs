﻿using System;
using System.Configuration;

namespace SharePointExecc
{
    public static class Extensions
    {
        

        public static string GetKeyValue(this string Key)
        {
            return ConfigurationManager.AppSettings[Key];
        }
    }
}
